Twilio conferencing module provides an easy way to add a block
where users can join a conference call.

Features:
  Block:
    The block can be assigned to whichever region is desired.
  Settings:
    Twillio standard settings plus recording enabled/disabled.
  Three permissions:
    - Speak - users are able to speak on conference calls or are always muted
    - Mute others - moderators can mute/unmute others
    - Join - grants access for users to join the conference call

Installation
------------

1) Create an entry at www.twilio.com/user/account/apps so Twilio can call your
site. The request URL should be: <your site URL>/twiml and the selected method
is POST. At the end of this step a SID is generated and that will be used in
the next step.

2) Download Twilo-PHP library and place its contents under the
library folder (sites/<all or site name>/twilio/).
Please note: The path sites/<all or site name>/twilio/Services/Twilio.php
must be valid.
Recommended version: https://github.com/twilio/twilio-php/zipball/3.5.1

3) Copy twilio_conferencing to your module directory and then enable on the
admin modules page.

4) Adjust the settings at admin/configuration/twilio_conferencing to reflect
your www.twilio.com account.

Author
------
Caio Luppi
caiovlp@gmail.com
