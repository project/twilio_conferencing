<?php

/**
 * @file
 * Administration forms for the twilio_conferencing module.
 */

/**
 * Admin form to configure Twilio settings.
 *
 * @return array
 *   The settings form.
 */
function twilio_conferencing_admin() {
  $form = array();

  $form['twilio_conferencing_account_sid'] = array(
    '#type' => 'textfield',
    '#title' => t('Account SID'),
    '#default_value' => variable_get('twilio_conferencing_account_sid'),
    '#size' => 64,
    '#maxlength' => 64,
    '#description' => t("The SID provided by Twilio when you create an account"),
    '#required' => TRUE,
  );

  $form['twilio_conferencing_auth_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Authentication Token'),
    '#default_value' => variable_get('twilio_conferencing_auth_token'),
    '#size' => 64,
    '#maxlength' => 64,
    '#description' => t("The authentication token provided by Twilio when you create an account"),
    '#required' => TRUE,
  );

  $form['twilio_conferencing_app_sid'] = array(
    '#type' => 'textfield',
    '#title' => t('Application SID'),
    '#default_value' => variable_get('twilio_conferencing_app_sid'),
    '#size' => 64,
    '#maxlength' => 64,
    '#description' => t("The application SID has to be create through Twilio website"),
    '#required' => TRUE,
  );

  $form['twilio_conferencing_record'] = array(
    '#type' => 'checkbox',
    '#title' => t('Record conferences'),
    '#description' => t('Enable recording of conference calls'),
    '#default_value' => variable_get('twilio_conferencing_record', FALSE),
  );

  return system_settings_form($form);
}
