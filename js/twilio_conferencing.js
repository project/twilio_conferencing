var user_token = null;
var token = null;
var check_for_updates_int_id = null;
var version = -1;

/**
 * Joins a conference call.
 */
function twilioConferencingJoinConference() {
  twilioConferencingSetOffline(false);
  twilioConferencingSetStatusClass('joining');
  Twilio.Device.setup(token);
}

/**
 * Leaves a conference call.
 */
function twilioConferencingLeaveConference() {
  twilioConferencingSetOffline(true);
  twilioConferencingSetStatusClass('offline');
  Twilio.Device.disconnectAll();
}

/**
 * Sets offline state.
 */
function twilioConferencingSetOffline(is_offline) {
  if (is_offline) {
    jQuery('div#conference-functions a.leave-conference').addClass('inactive');
    jQuery('div#conference-functions a.join-conference').removeClass('inactive');
    jQuery('div#conference-functions a.leave-conference').removeAttr('href');
    jQuery('div#conference-functions a.join-conference').attr('href', 'javascript:twilioConferencingJoinConference();');
  } else {
    jQuery('div#conference-functions a.leave-conference').removeClass('inactive');
    jQuery('div#conference-functions a.join-conference').addClass('inactive');
    jQuery('div#conference-functions a.leave-conference').attr('href', 'javascript:twilioConferencingLeaveConference();');
    jQuery('div#conference-functions a.join-conference').removeAttr('href');
  }
}

/**
 * Sets the proper status class.
 */
function twilioConferencingSetStatusClass(class_name) {
  jQuery('div#twilio-conference #status').removeClass('offline');
  jQuery('div#twilio-conference #status').removeClass('online');
  jQuery('div#twilio-conference #status').removeClass('joining');
  jQuery('div#twilio-conference #status').addClass(class_name);
}

/**
 * Performs an AJAX call to mute a participant.
 */
function twilioConferencingMuteParticipant(id, muted) {
  var path = 'twilio/mute-participant/' + user_token + '/' + id + '/' + muted;
  jQuery.ajax({
    url: Drupal.settings.basePath + path
  });
}

/**
 * Checks whether there were updates in the conference or not.
 */
function twilioConferencingCheckForUpdates() {
  jQuery.ajax({
    url: Drupal.settings.basePath + 'twilio/check-for-updates/' + version,
    dataType: 'json',
    success: function(data) {
      if (data != null) {
        version = data.version;
        twilioConferencingUpdateConferenceList(data);
      }
    }
  });
}

/**
 * Rebuilds the conference list with the updated data.
 */
function twilioConferencingUpdateConferenceList(data) {
  var html = '<div id="participants">';
  var user_uid = Drupal.settings.twilio_conferencing.uid;

  for (var i = 0; i < data.participants.length; i++) {
    var uid = data.participants[i].uid;
    var url = Drupal.settings.basePath + 'user/' + uid;
    var mute_img = '<img src="' + Drupal.settings.twilio_conferencing.mute_img + '" />';
    var unmute_img = '<img src="' + Drupal.settings.twilio_conferencing.unmute_img + '" />';
    var line = "odd";
    var mute = null;

    if (data.participants[i].muted == 0) {
      mute = mute_img;
      mute_flag = 'true';
    } else {
      mute = unmute_img;
      mute_flag = 'false';
    }

    if (data.participants[i].can_mute) {
      mute = "<a href='javascript:twilioConferencingMuteParticipant(" + uid + ", \"" + mute_flag + "\");'>" + mute + "</a>";
    }

    if (uid == user_uid) {
      line = "user-line";
    } else if ((i + 1) % 2 == 1) {
      line = "even";
    }

    html += '<div class="a-participant ' + line + '">';
    html += '<div class="participant-name"><a href="' + url + '">' + data.participants[i].name + '</a></div>';
    html += '<div class="participant-mute">' + mute + '</div>';
    html += '</div>';
  }

  html += '</div>';

  jQuery('#participants').html(html);
}

/**
 * Document.ready block.
 */
jQuery(document).ready(function() {
  user_token = Drupal.settings.twilio_conferencing.user_token;
  token = Drupal.settings.twilio_conferencing.token;

  Twilio.Device.ready(function (device) {
    jQuery('div#twilio-conference #status').text(Drupal.t('Connecting'));
    Twilio.Device.connect({
      user_token: user_token
    });
  });

  Twilio.Device.offline(function (device) {
    twilioConferencingSetOffline(true);
    twilioConferencingSetStatusClass('offline');
    jQuery('div#twilio-conference #status').text(Drupal.t('Offline'));
  });

  Twilio.Device.error(function (error) {
    twilioConferencingSetStatusClass('offline');
    twilioConferencingSetOffline(true);
    jQuery('div#twilio-conference #status').text(error);
  });

  Twilio.Device.connect(function (conn) {
    twilioConferencingSetStatusClass('online');
    twilioConferencingSetOffline(false);
    jQuery('div#twilio-conference #status').text(Drupal.t('Connected'));
  });

  Twilio.Device.disconnect(function (conn) {
    twilioConferencingSetOffline(true);
    twilioConferencingSetStatusClass('offline');
    jQuery('#status').text("Offline");
  });

  check_for_updates_int_id = setInterval("twilioConferencingCheckForUpdates()", 1000);
});
