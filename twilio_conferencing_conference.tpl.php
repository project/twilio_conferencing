<div id="twilio-conference">
  <div id="status" class="offline">Offline</div>
  <div id="conference-functions">
    <a href="javascript:twilioConferencingJoinConference();" class="join-conference">Join conference</a>
    <a class="leave-conference inactive">Leave conference</a>
  </div>
  <h2 class="conf-participants">Participants</h2>
  <div id="participants"></div>
</div>
