<?php

/**
 * @file
 * Auxiliary functions for the twilio_conferencing module.
 */

define('TWILIO_CONFERENCING_API_VERSION', '2010-04-01');

/**
 * Returns whether the user has access to join a coference.
 */
function twilio_conferencing_join_access() {
  return user_access('twilio_conferencing_conference_join');
}

/**
 * Returns whether the user has access to mute the user with a given uid.
 */
function twilio_conferencing_can_mute($uid) {
  global $user;

  if ($user->uid != $uid && !user_access('twilio_conferencing_conference_mute_others')) {
    // Unauthorized action: User is trying to mute other user but
    // has no permission to do so.
    return FALSE;
  }

  if ($user->uid == $uid && !user_access('twilio_conferencing_conference_speak')) {
    // Unauthorized action: User is trying to unmute itself but
    // has no permission to do so.
    return FALSE;
  }

  return TRUE;
}

/**
 * Return the content of the block and import the proper CSS and JS files.
 */
function twilio_conferencing_block_content() {
  if ($path = libraries_get_path('twilio')) {
    require_once $path . '/Services/Twilio/Capability.php';
  }

  // Don't display block if the user doesn't have permission to join.
  if (!twilio_conferencing_join_access()) {
    return "";
  }

  // Retrieve Twilio settings and generate a token.
  $account_sid = variable_get('twilio_conferencing_account_sid');
  $auth_token = variable_get('twilio_conferencing_auth_token');
  $app_sid = variable_get('twilio_conferencing_app_sid');

  $twilio = new Services_Twilio_Capability($account_sid, $auth_token);
  $twilio->allowClientOutgoing($app_sid);
  $token = $twilio->generateToken();

  // Retrieve the user token, or create a new if there isn't one yet.
  global $user;
  $user_token = _twilio_conferencing_get_user_token();
  $module_path = drupal_get_path('module', 'twilio_conferencing');
  $mute_path = base_path() . $module_path . '/img/mute.png';
  $unmute_path = base_path() . $module_path . '/img/unmute.png';

  $twilio_config = array(
    'twilio_conferencing' => array(
      'user_token' => $user_token,
      'token' => $token,
      'mute_img' => $mute_path,
      'unmute_img' => $unmute_path,
      'uid' => $user->uid,
    ),
  );

  drupal_add_js($twilio_config, 'setting');
  drupal_add_js('http://static.twilio.com/libs/twiliojs/1.0/twilio.js', 'external');
  drupal_add_js($module_path . '/js/twilio_conferencing.js');
  drupal_add_css($module_path . '/css/twilio_conferencing.css');

  return theme('twilio_conferencing_conference');
}

/**
 * Returns a token for a given user.
 */
function _twilio_conferencing_get_user_token() {
  global $user;

  $query = db_select('twilio_conferencing_conference', 'c');
  $query->condition('uid', $user->uid);
  $query->fields('c', array('user_token'));

  $result = $query->execute()->fetchAssoc();

  if (isset($result) && isset($result['user_token'])) {
    $user_token = $result['user_token'];
  }
  else {
    $user_token = md5($user->name . '-' . rand());

    $record = array(
      'uid' => $user->uid,
      'user_token' => $user_token,
      'is_active' => 0,
    );

    drupal_write_record('twilio_conferencing_conference', $record);
  }

  return $user_token;
}

/**
 * Menu callback that is called when the user is about to join a call.
 */
function twilio_conferencing_twiml() {
  $user_token = check_plain($_POST['user_token']);
  $call_sid = check_plain($_POST['CallSid']);

  // Update the call sid value and activate it.
  $query = db_update('twilio_conferencing_conference');
  $query->condition('user_token', $user_token);
  $query->fields(array('call_sid' => $call_sid, 'is_active' => 1, 'muted' => 0));
  $query->execute();

  _twilio_conferencing_set_new_version();

  $record = variable_get('twilio_conferencing_record', FALSE) ? 'true' : 'false';
  $mute = user_access('twilio_conferencing_conference_speak') ? 'false' : 'true';

  exit("<response><dial action=\"twilio/disconnect\" playBeep=\"true\" mute=\"$mute\" record=\"$record\"><conference>Client</conference></dial></response>");
}

/**
 * Menu callback that is called when the user is disconnected.
 */
function twilio_conferencing_disconnect() {
  $call_sid = check_plain($_POST['CallSid']);

  _twilio_conferencing_set_call_status($call_sid, 0);
  _twilio_conferencing_set_new_version();

  exit();
}

/**
 * Update the call status in the database.
 */
function _twilio_conferencing_set_call_status($call_sid, $is_active) {
  $query = db_update('twilio_conferencing_conference');
  $query->condition('call_sid', $call_sid);
  $query->fields(array('is_active' => $is_active));
  $query->execute();
}

/**
 * Returns the current version/state of the conference.
 */
function _twilio_conferencing_get_current_version() {
  $cached_version = cache_get('twilio_conferencing_current_version');

  if (isset($cached_version) && is_object($cached_version) && isset($cached_version->data)) {
    $current_version = $cached_version->data;
  }
  else {
    $current_version = 0;
  }

  if ($current_version > 10000) {
    $current_version = 0;
  }

  return $current_version;
}

/**
 * Set a new version/state of the conference.
 */
function _twilio_conferencing_set_new_version() {
  $current_version = _twilio_conferencing_get_current_version() + 1;
  cache_set('twilio_conferencing_current_version', $current_version);
}

/**
 * Menu callback that returns the updated conference JSON if there were changes.
 */
function twilio_conferencing_check_for_updates($version) {
  $current_version = _twilio_conferencing_get_current_version();
  $response = NULL;

  if ($current_version != $version) {
    $current_data = _twilio_conferencing_get_current_data($current_version);

    // Sets the can_mute for all users based on permissions.
    for ($i = 0; $i < count($current_data['participants']); $i++) {
      $can_mute = twilio_conferencing_can_mute($current_data['participants'][$i]['uid']);
      $current_data['participants'][$i]['can_mute'] = $can_mute;
    }

    $response = json_encode($current_data);
  }

  exit($response);
}

/**
 * Returns the current JSON with conference and participants data.
 */
function _twilio_conferencing_get_current_data($current_version) {
  $cached_data = cache_get('twilio_conferencing_current_data');

  if (isset($cached_data) && is_object($cached_data) && isset($cached_data->data)) {
    $data = $cached_data->data;
  }

  if (!isset($data) || $data['version'] != $current_version) {
    $data = array(
      'version' => $current_version,
      'participants' => _twilio_conferencing_get_participants(),
    );

    cache_set('twilio_conferencing_current_data', $data);
  }

  return $data;
}

/**
 * Retrieves a list of active participants.
 */
function _twilio_conferencing_get_participants() {
  $query = db_select('twilio_conferencing_conference', 'c');
  $query->join('users', 'u', 'c.uid = u.uid');
  $query->condition('is_active', 1);
  $query->fields('c');
  $query->fields('u');
  $result = $query->execute();

  $participants = array();
  foreach ($result as $record) {

    $participants[] = array(
      'name' => $record->name,
      'uid' => $record->uid,
      'muted' => $record->muted,
    );
  }

  return $participants;
}

/**
 * Menu callback that mutes/unmutes a given participant.
 */
function twilio_conferencing_mute_participant($user_token, $uid, $muted) {
  $account_sid = variable_get('twilio_conferencing_account_sid');
  $auth_token = variable_get('twilio_conferencing_auth_token');

  // Making sure the user is not trying to pass off as someone else.
  if (_twilio_conferencing_get_user_token() != $user_token) {
    return;
  }

  // Verify if user has permission to mute.
  if (!twilio_conferencing_can_mute($uid)) {
    return;
  }

  // Prevent ill-intentioned users from causing any damage by
  // changing the URL manually.
  if ($muted != 'true' && $muted != 'false') {
    $muted = 'false';
  }

  if ($path = libraries_get_path('twilio')) {
    require_once $path . '/Services/Twilio.php';
  }

  $client = new Services_Twilio($account_sid, $auth_token, TWILIO_CONFERENCING_API_VERSION);

  $response = $client->retrieveData("Accounts/$account_sid/Conferences?Status=in-progress&FriendlyName=Client", array(), FALSE);
  $conf_sid = $response->Conferences->Conference->Sid;

  $query = db_select('twilio_conferencing_conference', 'c');
  $query->condition('uid', $uid);
  $query->fields('c', array('call_sid'));

  $result = $query->execute()->fetchAssoc();
  $call_sid = $result['call_sid'];

  $response = $client->createData("Accounts/$account_sid/Conferences/$conf_sid/Participants/$call_sid", array('Muted' => $muted), FALSE);

  // Update the call sid value and activate it.
  $query = db_update('twilio_conferencing_conference');
  $query->condition('uid', $uid);
  $query->fields(array('muted' => ($muted == 'true' ? 1 : 0)));
  $query->execute();

  _twilio_conferencing_set_new_version();
}
